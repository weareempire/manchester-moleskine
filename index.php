<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="page--header">
<?php if ( is_single() ) : ?>
  <div class="intro post--published"><?php echo the_time( 'jS F Y' ); ?><br /><?php $categories = get_the_category(); foreach( $categories as $cat ) : echo '~ <a href="' . get_bloginfo( 'url' ) . '/category/' . $cat->slug . '" class="published--category">' . $cat->name . '</a> <br />'; endforeach; ?></div>
<?php endif; ?>
</div>

<div class="blog--content">

  <div class="content--inner">

    <div class="content--holder">

      <h1 class="post--header"><?php the_title(); ?></h1>

    </div>

  <?php if ( get_field( 'featured_image' ) ) : ?>
    <img src="<?php echo get_field( 'featured_image' ); ?>" alt="">
  <?php else : ?>
    <div style="height:200px;"></div>
  <?php endif; ?>

    <div class="content--holder main--content">

      <div class="main--content__inner">

      <?php the_content(); ?>

      </div>

      <?php if ( is_single() )
          {
            $currentID  = get_the_ID();
            $prev_post  = get_adjacent_post();
            $post_ids   = get_posts( array( $args, 'fields' => 'ids' ) );

            foreach ( array_keys( $post_ids, $currentID, true ) as $key )
            {
              unset( $post_ids[ $key ] );
            }
  
            $rand = array_rand( $post_ids );
            $ID = $post_ids[ $rand ]; ?>
      <div class="post--navigate"><?php if ( is_a( $prev_post, 'WP_Post' ) ) { ?><a href="<?php echo get_permalink( $prev_post->ID ); ?>">Next Post</a><?php } else { ?><a href="<?php echo get_permalink( $ID ); ?>">Next Post</a><?php } ?></div>
    <?php }?>

    </div>

  </div>

</div>

<?php endwhile; wp_reset_postdata(); ?>

<?php endif; ?>

<?php get_footer(); ?>
<?php /* ======================================================================

  IMPORT ADVANCED CUSTOM FIELDS

============================================================================ */

if ( function_exists( 'acf_add_options_page' ) ) {
  
  acf_add_options_page( array(
    'page_title'  => 'Empire Settings',
    'menu_title'  => 'EMP Settings',
    'menu_slug'   => 'general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
}


/* ============================================================================

  CLEAN WP HEADER

  - Removes new emoji settings

  - Remove Feed links

  - Remove Generator tags, hiding version number

  - Remove WooCommerce Stylesheets

============================================================================ */

function clean_wp_header() {

  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );

  remove_action( 'wp_head', 'feed_links_extra', 3 );
  remove_action( 'wp_head', 'feed_links', 2 );
  remove_action( 'wp_head', 'rsd_link' );
  remove_action( 'wp_head', 'wlwmanifest_link' );
  remove_action( 'wp_head', 'index_rel_link' );
  remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
  remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
  remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_head', 'wp_generator' );

  // add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

}

add_action( 'after_setup_theme', 'clean_wp_header' );


/* ============================================================================

  Further WordPress Cleanup

===============================================================================

  SOIL

  - Cleans up the head section in WordPress, linked with Soil plugin

  - Sanitize is now packaged with Soil, will now sanitize front end when soil is activated.

  - Sanitize strips out all unneeded white space.

============================================================================ */

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( is_plugin_active( 'soil-master/soil.php' ) ) :

  add_theme_support( 'soil-clean-up' );

  add_theme_support( 'soil-nice-search' );

  add_theme_support( 'soil-relative-urls' );

  add_theme_support( 'soil-disable-trackbacks' );

  add_theme_support( 'soil-disable-asset-versioning' );

  if ( !is_admin() ) :

    function sanitize_output( $buffer ) {

      $search = array(
        '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
        '/[^\S ]+\</s',  // strip whitespaces before tags, except space
        '/(\s)+/s'       // shorten multiple whitespace sequences
      );

      $replace = array(
          '>',
          '<',
          '\\1'
      );

      $buffer = preg_replace( $search, $replace, $buffer );

      return $buffer;

    }

    ob_start( 'sanitize_output' );

  endif;

endif;


/* ============================================================================

  ENQUEUE SCRIPTS AND STYLESHEETS

============================================================================ */

function wae_enqueue_scripts() {

// We don't need any of this in the dashboard so let's exclude
  if ( !is_admin() ) {

  // Import Stylesheet
    wp_enqueue_style( 'empire_base', get_stylesheet_directory_uri() . '/assets/css/main.css' );


  // Include jQuery
  
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '');
    wp_enqueue_script('jquery');
  

  // Include Modernizr for IE 8 and below
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr.js', array( 'jquery' ), false, false );
    wp_script_add_data( 'modernizr', 'conditional', 'lt IE 9' );

  // Check to see if we have minified js
    if ( file_exists( TEMPLATEPATH . '/assets/javascript/min' . '/javascript.min.js' ) )  :

    // Include min theme javscript
      wp_enqueue_script( 'javascript_min', get_stylesheet_directory_uri() . '/assets/javascript/min/javascript.min.js', array( 'jquery' ), false, true );

    else :

    // Include theme javascript
      wp_enqueue_script( 'javascript', get_stylesheet_directory_uri() . '/assets/javascript/javascript.js', array( 'jquery' ), false, true );

    endif;

    // if ( is_front_page() ) :

    //   wp_enqueue_script( 'fullpage', get_stylesheet_directory_uri() . '/assets/javascript/min/fullpage.min.js', array( 'jquery' ), false, true );  

    // endif;

  }

  wp_deregister_script( 'wp-embed.min.js' );
  wp_deregister_script( 'wp-embed' );
  wp_deregister_script( 'embed' );

}

add_action( 'wp_enqueue_scripts', 'wae_enqueue_scripts', 100 );


/* ============================================================================

  INCLUDE FAVICON AND APPLE TOUCH ICONS

============================================================================ */

function waeGetIcons() {

  if ( function_exists( 'acf_add_options_page' ) ) {

    if ( get_field( 'fav_ico', 'option' ) ) : echo '<link rel="shortcut icon" href="' . get_field( 'fav_ico', 'option' ) . '">'; endif;
    if ( get_field( 'apple_touch_logo', 'option' ) ) : echo '<link rel="apple-touch-icon" href="' . get_field( 'apple_touch_logo', 'option' ) . '">'; endif;

  }

}


/* ============================================================================

  INCLUDE TYPEKIT / GOOGLE FONTS

============================================================================ */

function waeGetFonts() {

  if ( function_exists( 'acf_add_options_page' ) ) {

    if ( get_field( 'google_fonts_link', 'option' ) ) : echo get_field( 'google_fonts_link', 'option' ); endif;
    if ( get_field( 'typekit_id', 'option' ) ) : ?>
      <script src="//use.typekit.net/<?php echo get_field( 'typekit_id', 'option' ); ?>.js"></script>
      <script>try{Typekit.load();}catch(e){}</script>
    <?php endif;

  }

}


/* ============================================================================

  GET GOOGLE ANALYTICSs

============================================================================ */

function waeGetAnalytics() {

  if ( function_exists( 'acf_add_options_page' ) ) {

    if ( get_field( 'google_analtics_id', 'option' ) ) : ?>
      
      <script>

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga( 'create', "<?php echo get_field( 'google_analtics_id', 'option' ); ?>", 'auto' );
        ga( 'send', 'pageview' );

      </script>

  <?php endif;

  }

}


/* ============================================================================

  WORDPRESS MENUS

===============================================================================

  - Register custom menus

  - Create Custom Walker, strips out unnecessary classes

============================================================================ */

register_nav_menus( array(

  'primary' => __( 'Primary', 'empire_base' ), 'secondary'  => __( 'Secondary', 'empire_base' ), 'footer'  => __( 'Footer', 'empire_base' )

) );


/* ============================================================================

  Custom Nav Walker

============================================================================ */

class custom_walker extends Walker_Nav_Menu {

  function start_el( &$output, $item, $depth = 0, $args = array() ) {

    global $wp_query;

    $indent = ( $depth ) ? str_repeat( '\t', $depth ) : '';
    
    $class_names = $value = '';

    $classes = empty( $item->classes ) ? array() : ( array ) $item->classes;

    $current_indicators = array( 'current-menu-item', 'current-menu-parent' );

    $newClasses = array();

    foreach( $classes as $el ) {

//check if it's indicating the current page, otherwise we don't need the class
      if ( in_array( $el, $current_indicators ) ) {

        array_push( $newClasses, $el );

      }

    }

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $newClasses ), $item ) );

    if ( $class_names != '' ) $class_names = ' class="active"';

    $output .= $indent . '<li' . $value . '>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

    if ( $depth != 0 ) {}

    $item_output = $args->before;
    $item_output .= '<a' . $attributes . '' . $class_names . '>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID );
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

  }

}




// FOOTER

function getFooter() {

  $args = array( 'posts_per_page' => -1 ); $myposts = get_posts( $args );

  $colour = array();

  foreach ( $myposts as $post ) : setup_postdata( $post );
    $id = $post->ID;
    if ( get_field( 'highlight_colour', $id ) ) : $colour[] = get_field( 'highlight_colour', $id ); endif;

  endforeach; wp_reset_postdata(); wp_reset_query();

  $pick = array_rand( $colour, 1 );

  $colour = $colour[$pick];

  echo '<div class="footer">';

    echo '<div class="row">';

      echo '<div class="footer--about">';

        echo '<h4>Hey, contribute to the project, ask a question or simply follow our journey.</h4>';
        echo '<p>When the dust has settled and everything is said and done the pair aims to auction off the original Moleskine® with all proceeds going to a charity based in the city they love.</p>';

      echo '</div>';

    echo '</div>';

    echo '<div class="row">';

      echo '<div class="footer--links">';

        echo '<div class="sponsor"></div>';
        echo '<div class="contact"> <a href="mailto:info@manchestermoleskin.co.uk">info@manchestermoleskin.co.uk <span style="background-color: ' . $colour . '"></span></a></div>';
        echo '<div class="social"> <a href="https://twitter.com/mcr_moleskine" target="_blank">Twitter <span style="background-color: #21aaf4;"></span></a> <a href="https://www.instagram.com/mcr_moleskine/" target="_blank">Instagram <span style="background-color: #e62d7c;"></span></a> <a href="https://www.facebook.com/events/408424749363683/" target="_blank">Facebook <span style="background-color: #5272b4;"></span></a> </div>';

      echo '</div>';

    echo '</div>';

    echo '<div class="footer--out">';

      echo '<div class="footer--logos">';

        echo '<div class="footer--logos__inner">';

        echo '<a href="#"> <img src="' . get_bloginfo('template_url') . '/assets/images/GFsmithlogo.png" alt=""> </a>';
        echo '<a href="#"> <img src="' . get_bloginfo('template_url') . '/assets/images/Foreverlogo.png" alt=""> </a>';

        echo '</div>';

      echo '</div>';

      echo '<div class="footer--links">';

        echo '<div class="contact alt"> <span>Made by</span> <a href="http://weareempire.co.uk" target="_blank">We Are Empire <span style="background-color: ' . $colour . '"></span></a></div>';

      echo '</div>';

    echo '</div>';

    echo '<div class="clear"></div>';

  echo '</div>';


}






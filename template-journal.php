<?php 

  /* Template Name: Page > Journal */

  get_header();

?>

<div class="page--header">
  <div class="intro">Every hero needs a journal. Moleskine® adventure updates can be found here.</div>
</div>

<?php $args = array( 'posts_per_page' => -1 ); $myposts = get_posts( $args ); ?>

<div class="blog--content is--list">

  <ul class="blog--list">

<?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

    <li> <div class="blog--item">
      <?php if ( get_field( 'post_thumbnail' ) ) : ?><a href="<?php echo get_permalink(); ?>"> <img src="<?php echo get_field( 'post_thumbnail' ); ?>" alt=""> </a><?php endif; ?>
      <div class="blog--snippet">
        <a class="post--link" href="<?php echo get_permalink(); ?>"> <?php echo the_title(); ?> </a>
        <div class="post--cat"><?php $categories = get_the_category(); foreach( $categories as $cat ) : echo '~ <a href="' . get_term_link( $cat->slug, 'category' ) . '">' . $cat->name . '</a> '; endforeach; ?></div>
        <?php if ( get_field( 'highlight_colour' ) ) : ?><span style="background-color:<?php echo get_field( 'highlight_colour' ); ?>;"></span><?php endif; ?>
      </div>
    </div> </li>

<?php endforeach; wp_reset_postdata(); wp_reset_query(); ?>

  </ul>

</div>

<?php get_footer(); ?>
$( document ).ready(function() {

  (function($,sr){

    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            };

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    }
    // smartresize 
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

  })(jQuery,'smartresize');

  function footerSize()
  {

    if ( $( window ).outerWidth(true) > 1024 )
    {
      about_height_min = $( '.footer--about', '.row' ).height();
      about_height = $( '.footer--about', '.row' ).outerHeight( true );
      links_height = $( '.footer--links', '.row' ).height();

      set_height = about_height + links_height;

      if ( $( '.footer--out' ).height() != set_height )
      {
        $( '.footer--logos' ).height(about_height_min);
        $( '.footer--out' ).height( set_height );
      }

    }
    else
    {
      $( '.footer--logos' ).removeAttr('style');
      $( '.footer--out' ).removeAttr('style');
    }

    $( window ).smartresize(function()
    {
      footerSize();
    });

  }

  footerSize();

  

  if ( $( 'body').hasClass('page-template-template-browse') )
  {
    $( 'html' ).addClass( 'allow-scroll' );
  }


// Homepage name changes

  var js_name = $( '.js-name' ),
      js_homepage = $( '.js-homepage--slider' );

  function changeName() {

    var target = $( this ),
        background = target.data( 'background' );

    if ( !target.hasClass( 'is-active' ) ) {

      target.addClass( 'is-active' ) // Make active
            .siblings( js_name ) // Find any other active name
            .removeClass( 'is-active' ); // Remove active class

      js_homepage.css({ 'background-image' : 'url(' + background + ')' }); // change background image based on target data background

    }

  }

  js_name.on( 'mouseenter', changeName );

// End Homepage name changes

  $( '.toggle--menu' ).on( 'click', function()
  {
    var text = 'Menu';

    if ( !$( '.navigation-list' ).hasClass('is-active') )
    {
      $( '.navigation-list' ).addClass('is-active');
      $( '.browse' ).addClass('is-active');
      $( this ).text('Close');
    }
    else
    {
      $( '.navigation-list' ).removeClass('is-active');
      $( '.browse' ).removeClass('is-active');
      $( this ).text(text);
    }

  });

});
<?php /* Template Name: Page > Browse */ get_header(); ?>

<?php if ( get_field( 'person' ) ) : $artists = get_field( 'person' ); ?>

<ul class="browse--all">

<?php

  $i = 1;

  $args = array( 'post_type' => array( 'post' ), 'posts_per_page' => -1 ); $myposts = get_posts( $args );

  foreach ( $artists as $artist ) :

    $colour = array();

  foreach ( $myposts as $post ) : setup_postdata( $post );
    $id = $post->ID;
    if ( get_field( 'highlight_colour', $id ) ) : $colour[] = get_field( 'highlight_colour', $id ); endif;
  endforeach; wp_reset_postdata(); wp_reset_query();

    $pick = array_rand( $colour, 1 );

    $colour = $colour[$pick];

?>

  <li<?php if ( $artist[ 'featured' ] || $i == 1 ) : echo ' class="full"'; endif; ?> id="<?php echo strtolower( str_replace( array( ' ', '\'', '-' ), '', $artist['name'][0]['last'] ) ); ?>">

    <img src="<?php echo $artist[ 'image' ]; ?>" alt="">

    <div class="artist--info">

    <?php if ( $artist['profile_image'] ) : ?>
      <img src="<?php echo $artist['profile_image']; ?>" alt="<?php echo $artist['name'][0]['first']; if ( $artist['name'][0]['last'] ) echo ' ' . $artist['name'][0]['last']; ?>">
    <?php endif; ?>

      <div class="artist--info__inner">
        <h3><?php echo $artist['name'][0]['first']; if ( $artist['name'][0]['last'] ) echo ' ' . $artist['name'][0]['last']; ?></h3>
        <?php if ( $artist['twitter_handle'] ) : ?><a href="https://twitter.com/<?php echo str_replace('@','',$artist['twitter_handle']); ?>" target="_blank"><?php echo $artist['twitter_handle']; ?> <span style="background-color:<?php echo $colour; ?>"></span> </a><?php endif; ?>
      </div>

    </div>

  </li>

<?php $i++; endforeach; ?>

</ul>

<?php endif; ?>

<?php get_footer(); ?>
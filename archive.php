<?php

  get_header();

  $category = get_category( get_query_var( 'cat' ) );
  $cat_id = $category->cat_ID;

?>

<div class="page--header">
  <div class="intro"><?php echo single_cat_title(); ?></div>
</div>

<?php $args = array( 'posts_per_page' => -1, 'category' => $cat_id ); $myposts = get_posts( $args ); ?>

<div class="blog--content">

  <ul class="blog--list">

<?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

    <li> <div class="blog--item">
      <?php if ( get_field( 'post_thumbnail' ) ) : ?><a href="<?php echo get_permalink(); ?>"> <img src="<?php echo get_field( 'post_thumbnail' ); ?>" alt=""> </a><?php endif; ?>
      <div class="blog--snippet">
        <a class="post--link" href="<?php bloginfo('url'); ?>/test-post"> <?php echo the_title(); ?> </a>
        <div class="post--cat"><?php $categories = get_the_category(); foreach( $categories as $cat ) : echo '~ <a href="' . get_bloginfo( 'url' ) . '/category/' . $cat->slug . '">' . $cat->name . '</a> '; endforeach; ?></div>
        <?php if ( get_field( 'highlight_colour' ) ) : ?><span style="background-color:<?php echo get_field( 'highlight_colour' ); ?>;"></span><?php endif; ?>
      </div>
    </div> </li>

<?php endforeach; wp_reset_postdata();?>

  </ul>

</div>

<?php get_footer(); ?>
<?php 

  /* Template Name: Page > Home */

  get_header();

  $browse_id = 6;

?>
 

  <?php if ( get_field( 'artists_repeater' ) ) : $artists = get_field( 'artists_repeater' ); $count = count($artists); $find = rand(1, ($count-1)); ?>

    <section class="section homepage--slider js-homepage--slider" style="background-image: url( '<?php echo $artists[$find-1]['artwork']; ?>' );" data-panel="begin">

    <div class="name--list">

    <?php $i = 1; foreach ( $artists as $artist ) : ?>

      <?php if ( $artist[ 'artist_name' ] && $artist[ 'artwork' ] ) : ?>

      <?php if ( $artist[ 'artist_name' ] !== 'All' ) : ?>

      <?php $target = get_bloginfo( 'url' ) . '/browse/#' . strtolower( str_replace( array( ' ', '\'', '-' ), '', $artist[ 'artist_name' ] ) ); ?>

      <?php else : ?>

      <?php $target = get_bloginfo( 'url' ) . '/browse/'; ?>

      <?php endif; ?>

      <a href="<?php echo $target; ?>" class="name js-name<?php if ( $i == $find ) : echo ' is-active'; endif; ?>" data-background="<?php echo $artist[ 'artwork' ]; ?>"><?php echo $artist[ 'artist_name' ]; ?></a><?php if ( $artist[ 'insert_line_break' ] ) : echo '<br />'; endif; ?>

      <?php endif; ?>

    <?php $i++; endforeach; ?>

    </div>

  </section>
  <?php endif; ?>

<?php get_footer(); ?>
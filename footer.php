
    <?php getFooter(); ?>

  <?php if ( is_front_page() ) : if ( get_field( 'artists_repeater' ) ) : $artists = get_field( 'artists_repeater' ); ?>
    <div class="preloader">
    <?php $i = 1; foreach ( $artists as $artist ) : if ( $artist[ 'artist_name' ] && $artist[ 'artwork' ] ) : ?>
      <div style="background-image: url( '<?php echo $artist[ 'artwork' ]; ?>' );"></div>
    <?php endif; $i++; endforeach; ?>
    </div>
  <?php endif; endif; ?>

    <?php wp_footer(); ?>

    <script> WebFontConfig = { google: { families: [ /*'Roboto:400,100,300,500,700,900:latin', */'Playfair+Display:400,400i:latin' ] } }; (function() { var wf = document.createElement('script'); wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js'; wf.async = 'true'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wf, s); })(); </script>

  </body>

</html>